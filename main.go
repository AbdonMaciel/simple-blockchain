package main

import (
	"crypto/sha256"
	"encoding/json"
	"fmt"
	"strconv"
	"strings"
	"time"
)

type Block struct {
	Data      map[string]interface{}
	Hash      string
	PrevHash  string
	TimeStamp time.Time
	Pow       int
}

type Blockchain struct {
	GenesisBlock Block
	Chain        []Block
	Difficulty   int
}

func (b *Block) calculateHash() string {
	data, _ := json.Marshal(b.Data)
	blockData := b.PrevHash + string(data) + b.TimeStamp.String() + strconv.Itoa(b.Pow)
	blockHash := sha256.Sum256([]byte(blockData))
	return fmt.Sprintf("%x", blockHash)
}

func (b *Block) mine(difficulty int) {
	for !strings.HasPrefix(b.Hash, strings.Repeat("0", difficulty)) {
		b.Pow++
		b.Hash = b.calculateHash()
	}
}

func (b *Blockchain) addBlock(from, to string, amount float64) {
	blockData := map[string]interface{}{
		"from":   from,
		"to":     to,
		"amount": amount,
	}
	lastBlock := b.Chain[len(b.Chain)-1]
	newBlock := Block{
		Data:      blockData,
		PrevHash:  lastBlock.Hash,
		TimeStamp: time.Now(),
	}
	newBlock.mine(b.Difficulty)
	b.Chain = append(b.Chain, newBlock)
}

func (b *Blockchain) isValid() bool {
	for i := range b.Chain[1:] {
		previousBlock := b.Chain[i]
		currentBlock := b.Chain[i+1]
		if currentBlock.Hash != currentBlock.calculateHash() || currentBlock.PrevHash != previousBlock.Hash {
			return false
		}

	}
	return true
}

func CreateBlockchain(difficulty int) Blockchain {
	genesisBlock := Block{
		Hash:      "0",
		TimeStamp: time.Now(),
	}
	return Blockchain{
		GenesisBlock: genesisBlock,
		Chain:        []Block{genesisBlock},
		Difficulty:   difficulty,
	}
}

func main() {
	blockchain := CreateBlockchain(2)

	blockchain.addBlock("Alice", "Bob", 1000)
	blockchain.addBlock("John", "Bob", 2000)

	for i, chain := range blockchain.Chain {

		fmt.Println("Index: ", i)
		fmt.Println("Hash: ", chain.Hash)
		fmt.Println("PreviousHash: ", chain.PrevHash)
		fmt.Println("From: ", chain.Data["from"])
		fmt.Println("To: ", chain.Data["to"])
		fmt.Println("Pow: ", chain.Pow)
		fmt.Println("-------------------------")
	}

	fmt.Println(blockchain.isValid())

}
